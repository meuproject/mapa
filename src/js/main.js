let neighborhoods = [];
let routes = [];
let markers = [];
let marker;
let markersRoute = [];
let markerRoute;
let map;
let infowindow;
console.log(routes);

function getToBank(data) {
    let position = data['data'];
    for (let i in position) {
        let atual = position[i];

        let positionAtual = atual["locations"][0];
        let latitude = positionAtual["latitude"];
        let longitude = positionAtual["longitude"];

        let car = {lat: latitude, lng: longitude, positions: []};

        for (let j in atual["locations"]) {
            let positionAtual = atual["locations"][j];
            let latitude = positionAtual["latitude"];
            let longitude = positionAtual["longitude"];
            routes.push({lat: latitude, lng: longitude, taxiNumber: atual["taxiNumber"]});

            car['positions'].push({lat: latitude, lng: longitude})
        }
        neighborhoods.push(car);
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: {lat: -3.7419423, lng: -38.5391274}
    });
}

function drop() {
    clearMarkers();
    for (var i = 0; i < neighborhoods.length; i++) {
        addMarkerWithTimeout(neighborhoods[i], i * 200);
    }
}

function route() {
    clearMarkersRoute();
    for (var i = 0; i < routes.length; i++) {
        addMarkerRoute(routes[i], i * 200);
    }
}

function addMarkerWithTimeout(position, timeout) {
    window.setTimeout(function () {
        markers.push(marker = new google.maps.Marker({
            position: position,
            map: map,
            animation: google.maps.Animation.DROP,
            icon: 'img/car-logo.png'
        }));

        marker.addListener('click', function () {
            let infowindow = new google.maps.InfoWindow({
                content: 'Taxi ' + position['taxiNumber']
            });
            infowindow.open(map, marker);
        });
    }, timeout);
}

function addMarkerRoute(position, timeout) {
    window.setTimeout(function () {
        markersRoute.push(markerRoute = new google.maps.Marker({
            position: position,
            map: map,
            animation: google.maps.Animation.DROP
            //icon: 'img/car-logo.png'
        }));
    }, timeout);
}

function clearMarkers() {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
    markers = [];
}

function clearMarkersRoute() {
    for (var i = 0; i < markersRoute.length; i++) {
        markersRoute[i].setMap(null);
    }
    markersRoute = [];
}

window.setTimeout(function () {
    document.getElementById("drop").click();
}, 2000);